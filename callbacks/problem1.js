const fs = require('fs');

function createDirectory(callback) {
    fs.mkdir('json_files', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("directory created");
        }
    })
    callback();
}

function createFile(callback) {
    fs.writeFile("json_files/obj1.json", '{"userId":1,"userName":"Josh"}', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("File created");
        }
    })
    callback();
}

function deleteFile() {
    fs.unlink('./json_files/obj1.json', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file deleted")

        }
    })
}

createDirectory(() => createFile(()=>deleteFile()))

module.exports=createDirectory;
module.exports=createFile;
module.exports=deleteFile;




