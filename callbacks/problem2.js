const fs = require("fs");

function readFileContent(cb) {
    fs.readFile('lipsum.txt', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            //console.log(data);
            console.log('file read')
        }
    })
    cb();
}


function toUpperCase(cb) {
    fs.readFile('lipsum.txt', (err, data) => {
        if (err) {
            console.log(err)
        } else {
            fs.writeFile("upper.txt", data.toString().toUpperCase(), (err) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log("uppercase file saved")
                    fs.writeFile('filenames.txt', 'upper.txt, ', (err) => {
                        console.log("filename saved")
                    })
                }
            })
        }
        cb();
    })
}

function getLowerCase(cb) {
    fs.readFile('upper.txt', 'utf-8', (err, data) => {
        if (err) {
            console.log(err)
        } else {
            fs.writeFile('lower.txt', data.toString().toLowerCase(), () => {
                console.log('lowerCase file saved')
            })
        }
        cb();
    })
}
function createSentenceFile(cb) {
    fs.readFile('lower.txt', 'utf-8', (err, data) => {
        if (err) {
            console.log(err)
        } else {
            fs.writeFile('sentences.txt', data.split('.').toString(), (err) => {
                console.log("sentences file saved")
                fs.appendFile('filenames.txt', 'sentences.txt, ', (err) => {
                    console.log("filename saved")
                })
            });

        }
        cb();
    })
}

function readAndSort(cb) {
    fs.readFile('sentences.txt', 'utf-8', (err, data) => {
        if (err) {
            console.log(err)
        } else {
            fs.writeFile('sorted.txt', data.split(' ').sort().toString(), (err) => {
                console.log("sorted file Saved")
                fs.appendFile('filenames.txt', 'sorted.txt, ', (err) => {
                    console.log('filename saved')
                })
            })
        }
        cb()
    })
}

function deleteFilesContent() {
    fs.readFile("filenames.txt", 'utf-8', (err, data) => {
        if (err) {
            console.log(err)
        } else {
            fs.truncate('filenames.txt', (err) => {
                console.log("filesContent deleted")
            })
        }
    })
}

readFileContent(() => toUpperCase(() => getLowerCase(() => createSentenceFile(() => readAndSort(() => deleteFilesContent())))))


module.exports = readFileContent;
module.exports = toUpperCase;
module.exports = getLowerCase;
module.exports = createSentenceFile;
module.exports = readAndSort;
module.exports = deleteFilesContent;
