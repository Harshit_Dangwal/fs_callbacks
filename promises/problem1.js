const fs = require("fs");
const { json } = require("stream/consumers");

function createDirectory() {
    return new Promise((resolve, reject) => {
        fs.mkdir("jsonFiles", (err) => {
            if (err) {
                reject(err);
            } else {
                resolve("directory created");
            }
        })
    })
};

function createFiles() {
    return new Promise((resolve, reject) => {
        let files = { "obj1.json": { "a": true }, "obj2.json": { "b": false } };
        let c = 0;
        for (i in files) {
            fs.writeFile("./jsonFiles/" + i, JSON.stringify(files[i]), (err) => {
                c += 1
                if (err) {
                    reject(err);
                } else if (c === Object.keys(files).length) {
                    resolve("json file created");
                }
            })
        }
    })
};

function deleteFiles() {
    return new Promise((resolve, reject) => {
        fs.readdir("jsonFiles", (err, data) => {
            if (err) {
                reject(err);
            } else {
                let c = data.length;
                for (i of data) {
                    fs.unlink("./jsonFiles/" + i, (err) => {
                        c -= 1
                        if (err) {
                            reject(err)
                        } else if (c === 0) {
                            resolve("files deleted");
                        }
                    })
                }
            }
        })
    })
};

createDirectory().then((msg) => {
    console.log(msg)
    return createFiles()
}).then((msg) => {
    console.log(msg)
    return deleteFiles()
}).then((msg) => {
    console.log(msg)
});

