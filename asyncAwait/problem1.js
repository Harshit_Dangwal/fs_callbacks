const fs = require("fs");

function createDirectory() {
    return new Promise((resolve, reject) => {
        fs.mkdir("jsonFiles", (err) => {
            if (err) {
                reject(err);
            } else {
                resolve("directory created");
            } 
        })
    })
};

function createFiles() {
    return new Promise((resolve, reject) => {
        let files = { "obj1.json": { "a": true }, "obj2.json": { "b": false } };
        for (i in files) {
            fs.writeFile("./jsonFiles/" + i, JSON.stringify(files[i]), (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve("json files created");
                }
            })
        }
    })
}

function deleteFiles() {
    return new Promise((resolve, reject) => {
        fs.readdir("jsonFiles", (err, data) => {
            if (err) {
                reject(err);
            } else {
                for (i of data) {
                    fs.unlink("./jsonFiles/" + i, (err) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve("files deleted")
                        }
                    })
                }
            }
        })
    })
}

async function getData(){
    try{
    const directory=await createDirectory();
    console.log(directory);
    const jsonFiles = await createFiles();
    console.log(jsonFiles);
    const deletedFiles = await deleteFiles();
    console.log(deletedFiles)
    }catch(error){
        console.log(error);
    }
}

getData();
