const fs = require("fs");

function crud() {
    const readTextFile = new Promise((resolve, reject) => {
        fs.readFile('lipsum.txt', 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                //console.log(data);
                resolve("file read");
            }
        })
    })

    const toUpperCase = new Promise((resolve, reject) => {
        fs.readFile('lipsum.txt', 'utf-8', (err, data) => {
            if (err) {
                reject(error);
            } else {
                fs.writeFile("upper.txt", data.toString().toUpperCase(), (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        fs.writeFile('filenames.txt', 'upper.txt', (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve("files upper.txt created and saved to filenames");
                            }
                        })
                    }
                })
            }
        })
    })

    const getLowerCaseAndSplit = () => {
        return new Promise((resolve, reject) => {
            fs.readFile('upper.txt', 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    fs.writeFile('upper.txt', data.toString().toLowerCase(), () => {
                    })
                    fs.readFile('upper.txt', 'utf-8', (err, data) => {
                        if (err) {
                            reject(err);
                        } else {
                            fs.writeFile('sentences.txt', data.split('.').toString(), (err) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    fs.writeFile('filenames.txt', 'sentences.txt', (err) => {
                                        resolve("updated file content and saved to filenames")
                                    })
                                }
                            });

                        }
                    })

                }
            })
        })
    }

    const readAndSort = () => {
        return new Promise((resolve, reject) => {
            fs.readFile('sentences.txt', 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    fs.writeFile('sorted.txt', data.split(' ').sort().toString(), (err) => {
                        if (err) {
                            reject(err)
                        } else {
                            fs.writeFile('filenames.txt', "sorted.txt", (err) => {
                                resolve("file read,sorted and saved to filenames")
                            })
                        }
                    })

                }
            })
        })
    }

    const readAndDelete = () => {
        return new Promise((resolve, reject) => {
            fs.readFile("filenames.txt", 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    fs.truncate('filenames.txt', (err) => {
                        resolve("filesContent deleted");
                    })
                }
            })
        })
    }
    async function getData(){
        try{
            const readFile = await readTextFile;
        console.log(readFile);
        const uppercase = await toUpperCase;
        console.log(uppercase);
        const lowerCase = await getLowerCaseAndSplit();
        console.log(lowerCase);
        const sorted = await readAndSort();
        console.log(sorted);
        const deleted = await readAndDelete();
        console.log(deleted);
        }catch(error){
            console.log(error);
        }
    }
    getData();
}

crud();
